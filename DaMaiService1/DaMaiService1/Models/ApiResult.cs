﻿namespace DaMaiService1.Models
{
    public class ApiResult
    {
        public int Code { get; set; }

        public string Message { get; set; }

        public ApiResult()
        {
        }

        public ApiResult(int code, string message)
        {
            Code = code;
            Message = message;
        }
    }

    public class ApiResult<T> : ApiResult
    {
        public T Data { get; set; }

        public ApiResult()
        {
        }

        public ApiResult(int code, string message) : base(code, message)
        {
        }
    }
}
