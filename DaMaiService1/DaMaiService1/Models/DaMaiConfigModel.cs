﻿using System.ComponentModel.DataAnnotations;

namespace DaMaiService1.Models;

public class DaMaiConfigModel
{
    [Key]
    public int Id { get; set; }

    public string Description { get; set; } = "";

    public string Name { get; set; } = "";

    public string Region { get; set; }

    public string DateTime { get; set; }

    public string Price { get; set; }

    public int Ticket { get; set; }

    public string? User { get; set; }

    public string Pay { get; set; } = "微信";

    public string DoOrder { get; set; } = "是";

    public string ExecTime { get; set; }
}
