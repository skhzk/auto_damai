using DaMaiService1.Repository;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

// add swagger
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// sqlite
var sqlFileName = Path.Combine(AppContext.BaseDirectory, $"{typeof(Program).Assembly.GetName().Name ?? "default"}.db");
var sqlConnectionString = $"Filename={sqlFileName}";
builder.Services.AddDbContext<SqliteDbContext>(options =>
{
    options.UseSqlite(sqlConnectionString);
});

var app = builder.Build();

// sqlite ������Ǩ��
using var scope = app.Services.CreateScope();
var db = scope.ServiceProvider.GetService<SqliteDbContext>();
db?.Database.Migrate();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

// swagger
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;

app.Run();
