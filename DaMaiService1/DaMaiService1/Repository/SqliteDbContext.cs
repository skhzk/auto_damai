﻿using DaMaiService1.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Principal;

namespace DaMaiService1.Repository;

public class SqliteDbContext : DbContext
{
    public DbSet<DaMaiConfigModel> DaMaiConfig { get; set; }

    public SqliteDbContext(DbContextOptions<SqliteDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
    }
}
