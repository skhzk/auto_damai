﻿using DaMaiService1.Models;
using DaMaiService1.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DaMaiService1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DaMaiController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly SqliteDbContext _context;

        public DaMaiController(ILogger<DaMaiController> logger,SqliteDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public List<DaMaiConfigModel> GetList()
        {
            var list = _context.DaMaiConfig.ToList();

            return list;
        }

        [HttpGet]
        public ApiResult<DaMaiConfigModel?> Get(string description)
        {
            _logger.LogInformation(description);

            var entity = _context.DaMaiConfig.FirstOrDefault(x => x.Description == description);

            var result = new ApiResult<DaMaiConfigModel?>();

            if(entity != null)
            {
                result.Code = 200;
                result.Message = "successfully";
                result.Data = entity;
            }
            else
            {
                result.Code = 404;
                result.Message = "fail";
                result.Data = null;
            }

            return result;
        }

        [HttpPost]
        public ApiResult Save(DaMaiConfigModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Description))
            {
                return new ApiResult(400, "描述不能为空");
            }
            if (_context.DaMaiConfig.Any(s => s.Id != model.Id && s.Description == model.Description))
            {

                return new ApiResult(400, "存在相同描述的记录");
            }

            if(model.Id > 0)
            {
                var entity = _context.DaMaiConfig.FirstOrDefault(s => s.Id == model.Id);

                if(entity == null)
                {
                    return new ApiResult(400, "记录不存在，更新失败");
                }

                entity.Name = model.Name ?? "";
                entity.Description = model.Description ?? "";
                entity.Region = model.Region ?? "";
                entity.DateTime = model.DateTime ?? "";
                entity.Price = model.Price ?? "";
                entity.Ticket = model.Ticket;
                entity.User = model.User ?? "";
                entity.Pay = model.Pay ?? "";
                entity.DoOrder = model.DoOrder ?? "";
                entity.ExecTime = model.ExecTime ?? "";

                _context.Update(entity);
            }
            else
            {
                model.Name = model.Name ?? "";
                model.Description = model.Description ?? "";
                model.Region = model.Region ?? "";
                model.DateTime = model.DateTime ?? "";
                model.Price = model.Price ?? "";
                model.Ticket = model.Ticket;
                model.User = model.User ?? "";
                model.Pay = model.Pay ?? "";
                model.DoOrder = model.DoOrder ?? "";
                model.ExecTime = model.ExecTime ?? "";

                _context.Add(model);
            }

            _context.SaveChanges();

            return new ApiResult(200, "成功");
        }

        [HttpGet]
        public ApiResult Delete(int id)
        {
            var entity = _context.DaMaiConfig.FirstOrDefault(s => s.Id == id);

            if(entity == null)
            {
                return new ApiResult(400, "删除对象不存在");
            }

            _context.DaMaiConfig.Remove(entity);
            _context.SaveChanges();

            return new ApiResult(200, "成功");
        }
    }
}
