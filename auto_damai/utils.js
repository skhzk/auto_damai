// 不能命名为util，会根autojs原生util冲突
utils = {}

// 获取自适应宽度
utils.getScaleWith = function(width) {
  return utils.getTwoScale(device.width / 900) * width; 
}

// 获取自适应高度
utils.getScaleHeight = function(height) {
  return utils.getTwoScale(device.height / 1600) * height; 
}

// 获取两位小数
utils.getTwoScale = function(num) {
  return Math.floor(num * 100) / 100;
}

// 组件5s内循环判断
utils.componentExist = function(compnnent) {
  let count = 0;
  while(count < 500) {
    if(compnnent.exists()) {
      return true;
    }
    count++;
    sleep(10);
  }
  console.log("组件寻找异常，即将结束自动化脚本")
  return false;
}

utils.init = function(){
  // 判断是否有无障碍模式
  auto.waitFor()
  console.show()
}

utils.unlock = function(){
  // 解屏
  swipe(this.getScaleWith(850), this.getScaleHeight(1400), this.getScaleWith(100), this.getScaleHeight(1400), this.getScaleWith(100))
}

utils.tryAactionCick = function(compnnent, lx, ly, rx, ry) {
  let count = 0;
  while(count < 500) {
    if(compnnent.exists()) {
      console.log("按钮已经出现")
      return true;
    }
    console.log("重新点击")
    click(lx, ly, rx, ry);
    count++;
    sleep(10);
  }
  console.log("组件寻找异常，即将结束自动化脚本")
  return false;
}

utils.tryAaction = function(compnnent1, compnnent2) {
  let count = 0;
  while(count < 500) {
    if(compnnent1.exists()) {
      console.log("按钮已经出现")
      return true;
    }
    console.log("重新点击")
    try{
      compnnent2.parent().click()
    }
    catch (e) {

    }
    count++;
    sleep(10);
  }
  console.log("组件寻找异常，即将结束自动化脚本")
  return false;
}
module.exports = utils