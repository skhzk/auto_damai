"ui";
let damai = require("./damai.js")
let utils = require("utils");
showLoginUI();
ui.statusBarColor("#000000")

//显示登录界面
function showLoginUI(){
    ui.layout(
    <ScrollView>
      <frame>
        <vertical h="auto" align="center" margin="0 50">
          <linear>
             <text gravity="center" color="#111111" size="16">演唱名称必填，其他可以非必填（按预约默认需要清空）</text>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">描述（用于云端拉取）：</text>
             <input id="desc" w="*" h="80" text="周杰伦1"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">演唱会名称：</text>
             <input id="name" w="*" h="80" text="戴佩妮"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">地区：</text>
             <input id="region" w="*" h="80" text="广州"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">票档：</text>
             <input id="time" w="*" h="80" text="2023-10-21  周六 20:00"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">票数：</text>
             <input id="ticket" w="*" h="80" text="2"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">价格：</text>
             <input id="price" w="*" h="80" text="980元"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">观看人：</text>
             <input id="user" w="*" h="80" text=""/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">支付方式：</text>
             <input id="pay" w="*" h="80" text="微信"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">是否下单：</text>
             <input id="doOrder" w="*" h="80" text="否"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">第一次执行时间：</text>
             <input id="exeTime" w="*" h="80" text="2023-08-27 21:34:00"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">是否定时执行（按天）：</text>
             <input id="repeat" w="*" h="80" text="N"/>
          </linear>
          <linear>
             <text w="100" gravity="center" color="#111111" size="16">锁屏密码：</text>
             <input id="password" w="*" h="80" password="true"/>
          </linear>
          <linear gravity="center">
             <button id="login" text="初始化"/>
             <button id="load" text="云端加载"/>
             <button id="register" text="执行"/>
          </linear>
        </vertical>
      </frame>
      </ScrollView>
    );

    ui.login.on("click", () => {
      threads.start(function(){
        //在新线程执行的代码
        utils.init();
        toast("初始化成功");
      });
    
    });
    ui.load.on("click", () => {
	  desc = ui.desc.text()
	  threads.start(function(){
		var res = http.get(`http://47.101.134.134:7123/api/DaMai/Get?description=${desc}`);
		if(res.statusCode == 200) {
			var resJson = res.body.json();
			var data = resJson.data
			log(data);
			// 由于不能在子线程操作UI，所以要抛到UI线程执行
			ui.post(() => {
				ui.exeTime.setText(data.execTime)
				ui.name.setText(data.name)
				ui.region.setText(data.region)
				ui.time.setText(data.dateTime)
				ui.price.setText(data.price)
				ui.ticket.setText(data.ticket.toString())
				ui.user.setText(data.user)
				ui.pay.setText(data.pay)
				ui.doOrder.setText(data.doOrder)
			});
		}
			
	  });
    });
    ui.register.on("click", () => {
        config = {}
        config.time = ui.exeTime.text()
        config.isRepeat = (ui.repeat.text() == 'Y' ? true : false)
        config.password = ui.password.text()
        damai._config.name = ui.name.text()
        damai._config.region = ui.region.text()
        damai._config.time = ui.time.text()
        damai._config.price = ui.price.text()
        damai._config.ticket = ui.ticket.text()
        damai._config.user = ui.user.text()
        damai._config.pay = ui.pay.text()
        damai._config.doOrder = ui.doOrder.text()
        console.log(damai._config.name)
        console.log(damai._config.region)
        console.log(damai._config.time)
        console.log(damai._config.price)
        console.log(damai._config.ticket)
        console.log(damai._config.user)
        console.log(damai._config.pay)
        console.log(damai._config.doOrder)
        damai.exec(config)
      }
    );
}

// 主逻辑
function main() {
    damai._config.name = "戴佩妮"
    damai._config.region = "广州"
    damai._config.time = "2023-10-21  周六 20:00"
    damai._config.price = "980元"
    damai._config.ticket = 2
    damai._config.user = ""
    damai._config.pay = "微信"
    damai._config.doOrder = "否"
    //damai.prepare()
    //damai.exec()
}


