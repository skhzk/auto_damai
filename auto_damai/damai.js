let timer = require("./timer.js");
let utils = require("./utils.js");


damai = {}

damai._config = {}


damai.exec = function(param) {
  threads.start(function(){
    //在新线程执行的代码
    let config = {}
    config.exeTime = timer.getDate(param.time)
    config.isRepeat = param.isRepeat
    config.execution = execution
    config.prepare = prepare
    config.password = param.password
    timer.timeExecute(config)  
  });

}

function prepare() {
  // 启动应用
  launch("cn.damai")
  sleep(1000)
  // 点击搜索
  id("homepage_header_search_btn").click()
  id("header_search_v2_input").setText(damai._config.name);
  if(!utils.componentExist(id("tv_word"))) {
    console.log("搜索异常")
    return
  } 
  sleep(2000)
  // 进入下拉列表项
  //click(utils.getScaleWith(250), utils.getScaleHeight(175), utils.getScaleWith(280), utils.getScaleHeight(215))
  let tvWord = id("tv_word").findOne(1500)
  click(tvWord.bounds().centerX(), tvWord.bounds().centerY())
  sleep(2000)

  if(damai._config.region != '' && damai._config.region != undefined) {
    let item = undefined;
    id("tv_project_name").untilFind().forEach(element => {
      console.log(element.text())
      console.log(element.text().indexOf(damai._config.region) )
      if(element.text().indexOf(damai._config.region) != -1) {
        item = element;
      }
    });
    if(item == undefined) {
      console.log("无法找到指定地区演唱会")
      return
    }
    console.log(item.text())
    click(item.bounds().centerX(), item.bounds().centerY())
  } else {
    click(utils.getScaleWith(250), utils.getScaleHeight(175), utils.getScaleWith(280), utils.getScaleHeight(215))
    /*while(!id("ll_search_item").exists()) {
      console.log("search repeat")
      click(utils.getScaleWith(250), utils.getScaleHeight(175), utils.getScaleWith(280), utils.getScaleHeight(215))
      sleep(2000)
    }*/
  }
  sleep(2000)
  // 进入详情
  click(utils.getScaleWith(250), utils.getScaleHeight(330), utils.getScaleWith(280), utils.getScaleHeight(350))
  /*while(!id("project_price_tv").exists()) {
    console.log("item repeat")
    click(utils.getScaleWith(250), utils.getScaleHeight(330), utils.getScaleWith(280), utils.getScaleHeight(350))
    sleep(3000)
  }*/
  while(!utils.componentExist(id("project_price_tv"))) {
    sleep(50);
  }
}

function execution() {
  
  // 点击购买
  console.log("准备购买")
  id("tv_left_main_text").findOne().parent().click()
  //click(utils.getScaleWith(515), utils.getScaleHeight(1530), utils.getScaleWith(520), utils.getScaleHeight(1535))
  //utils.tryAactionCick(id("tv_title"), utils.getScaleWith(515), utils.getScaleHeight(1530), utils.getScaleWith(520), utils.getScaleHeight(1535))
  // 票档
  if(damai._config.time != undefined) {
    console.log(damai._config.time)
    //id("item_text").untilFind().forEach(element => {
    //  console.log(element.text())
    //  console.log(element.text().indexOf(damai._config.time) )
    //  if(element.text().indexOf(damai._config.time) != -1) {
    //    click(element.bounds().centerX(), element.bounds().centerY())
    //  }
      //element.parent().click()
    // });  
    text(damai._config.time).untilFind().forEach(element => {
      console.log("time click")
      element.parent().click()
    });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  }
  // 价格
  if(damai._config.price != undefined) {
    console.log(damai._config.price)
    /*id("item_text").untilFind().forEach(element => {
      console.log(element.text())
      console.log(element.text().indexOf(damai._config.price) )
      if(element.text().indexOf(damai._config.price) != -1) {
        click(element.bounds().centerX(), element.bounds().centerY())
      }
      //element.parent().click()
    });*/
    text(damai._config.price).untilFind().forEach(element => {
      console.log("price click")
      element.parent().click()
    });
    //console.log(text("980元").bounds().centerX())
    //utils.tryAaction(id("img_jia"), text(damai._config.price))
    //console.log("have find")
  }
  // 票数
  if(damai._config.ticket != undefined) {
    let number = parseInt(damai._config.ticket)
    for(let i = 1; i <= number - 1; i++) {
      id("img_jia").findOne().click()
    }
  }

  id("btn_buy").findOne().click()
  // 观众
  if(damai._config.user != undefined) {
    console.log("观众")
    let users = damai._config.user.split(",")
    let textName = id("text_name").untilFind()
    let checkbox = id("checkbox").untilFind()
    for(let i = 0; i < textName.length; i++) {
      users.forEach(u => {
        console.log(textName[i].text())
        if(textName[i].text().indexOf(u) != -1) {
          console.log("seer")
          checkbox[i].click()
        }
      })
    }
  }
  // 支付方式
  if(damai._config.pay != undefined) {
    console.log("支付方式")
    let checkbox = id("cb_pay_check").untilFind()
    let pay = id("tv_pay_title").untilFind()
    for(let i = 0; i < pay.length; i++) {
      console.log(pay[i].text())
      if(pay[i].text().indexOf(damai._config.pay) != -1) {
        click(checkbox[i].bounds().centerX(), checkbox[i].bounds().centerY())
      }
    }
  }

  //console.log(className("android.widget.TextView").text("提交订单").findOne().bounds().centerX())
  //下单
  if(damai._config.doOrder == '是' ) {
    className("android.widget.TextView").text("提交订单").findOne().findOne().click();
  }
}


module.exports = damai